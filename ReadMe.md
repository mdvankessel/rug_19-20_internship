# Read Me

This repository is a complemantary part of the report: 'The Influence of inflammation on the gut microbial community of Groninger Rats', by Maarten van Kessel.

## Files
```bash
data/
    ank_feature-table.txt
    ank_metadata.tsv
    ank_rooted-tree.nwk
    ank_taxonomy.tsv
    ank_unrooted-tree.nwk
    ank_micro_biome_behaviour.csv
docs/
    Log_Markdown.pdf
scripts/
    R/
        Log_Markdown.Rmd
    bash/
        qiime_merged.sh
        qiime_p1.sh
        qiime_p2.sh
```

## Author
Maarten van Kessel