#!/bin/bash
#SBATCH --job-name=qiime_Ank-p1
#SBATCH --time=03:00:00
#SBATCH --cpus-per-task=5
#SBATCH --mem=8GB
#SBATCH --nodes=10
#SBATCH --ntasks-per-node=1
#SBATCH --partition=regular
#SBATCH --output=/data/s4215494/POCD_project/logs/qiime_Ank-p1-%j.log
#SBATCH --mail-type ALL
#SBATCH --mail-user m.d.van.kessel@student.rug.nl

module load QIIME2/2019.4

# VARIABLES
## General data directory
data_dir='/data/s4215494/POCD_project/'

## [input] .fna File name
input_dir=$data_dir'raw_data/data_Ank/'

## [output] .qza File name
output_dir=$data_dir'Ank_output/'
output_import=$output_dir'output_import_fastq/'
output_demux=$output_dir'output_cutadapt_demux-single/'
output_dada2=$output_dir'output_dada2/'

# Import
qiimeToolsImport () {
    qiime tools import \
    --type MultiplexedSingleEndBarcodeInSequence \
    --input-path $input_dir'p1_forward.fastq.gz' \
    --output-path $output_import'p1-Ank'
}

##### Demultiplex #####
# --time=03:00:00
# --cpus-per-task=1
# --mem=8GB
# --nodes=1
# --ntasks-per-node=1
# NOTE: No multithreading possible (software constraints).
# Run Time: < 2 hours.

qiimeCutadaptDemuxSingle () {
    qiime cutadapt demux-single \
    --i-seqs $output_import'p1-Ank.qza' \
    --m-barcodes-file ~/metadata_final-p1.tsv \
    --m-barcodes-column BarcodeSequence \
    --o-per-sample-sequences $output_demux'p1-Ank_demuxed' \
    --o-untrimmed-sequences $output_demux'p1-Ank_no-match'
}

##### Dereplicate / Filter Chimera / Denoise #####
qiimeDadaDenoiseSingle () {
    qiime dada2 denoise-single \
    --i-demultiplexed-seqs $output_demux'p1-Ank-demuxed' \
    --p-trunc-len 192 \
    --p-n-threads 5 \
    --o-table $output_dada2'filtered-table' \
    --o-representative-sequences $output_dada'filtered-seqs' \
    --o-denoising-stats $output_dada'filtered-stats' \
}

# Comment out any of the below function calls to run part of the script.
qiimeToolsImport
qiimeCutadaptDemuxSingle
qiimeDadaDenoiseSingle