#!/bin/bash
#SBATCH --job-name=full_pipe
#SBATCH --time=00:10:00
#SBATCH --cpus-per-task=10
#SBATCH --mem=12GB
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=regular
#SBATCH --output=/data/s4215494/POCD_project/logs/qiime_Ank-p1-%j.log
#SBATCH --mail-type ALL
#SBATCH --mail-user m.d.van.kessel@student.rug.nl


#======================================================================================
# 
#        FILE: qiime_merged.sh
#
#       USAGE: sbatch qiime_merged.sh
# 
# DESCRIPTION: - Merges sequence pools
#              - Merges table pools
#              - Clusters OTUs
#              - Creats Taxonomy table
#              - Creates Phylogenic tree
#
#      AUTHOR: Maarten van Kessel
#     CREATED: 10.10.2019
#     VERSION: 1.0
#=====================================================================================

#-------------------------------------------------------------------------------------
# Add QIIME 2 version 2019.4 to module list in SLURM enviorment on Peregrine.
#-------------------------------------------------------------------------------------
module load QIIME2/2019.4

#--------------------------------------------------------------------------------------
# VARIABLES
#--------------------------------------------------------------------------------------
data_dir='/data/s4215494/POCD_project/'

classifier=$data_dir'databases/gg-13-8-99-515-806-nb-classifier.qza'
dataset_one='Ank'
dataset_two='Nat-Nep'
pool_one='p1'
pool_two='p2'
pool_three='p3'

source /home/s4215494/bash_scripts/config.sh


#===  Function   ======================================================================
#        NAME: qiimeFeatureTableMergeTable
# DESCRIPTION: Merges different table pools into one
#      SBATCH: --cpus-per-task=1
#              --mem=12GB
#              --nodes=1
#              --ntasks-per-node=1
#         ETA: 10 minutes.
#======================================================================================
qiimeFeatureTableMergeTable () {
    qiime feature-table merge \
    --i-tables $output_dada'Ank-p1-table.qza' \
    --i-tables $output_dada'Ank-p2-table.qza' \
    --o-merged-table $output_merge'Ank_p1-2_merged-table'
}


#===  Function   ======================================================================
#        NAME: qiimeFeatureTableMergeSeqs
# DESCRIPTION: Merges different table sequences into one
#      SBATCH: --cpus-per-task=1
#              --mem=12GB
#              --nodes=1
#              --ntasks-per-node=1
#         ETA: 10 minutes.
#======================================================================================
qiimeFeatureTableMergeSeqs () {
    qiime feature-table merge-seqs \
    --i-data $output_dada'Ank-p1-seqs.qza' \
    --i-data $output_dada'Ank-p2-seqs.qza' \
    --o-merged-data $output_merge'Ank_p1-2_merged-seqs'
}


#===  Function   ======================================================================
#        NAME: qiimeVSearchClusterFeaturesDeNovo
# DESCRIPTION: Clusters OTU's de-novo style.
#      SBATCH: --cpus-per-task=5
#              --mem=12GB
#              --nodes=10
#              --ntasks-per-node=1
#         ETA: 3 hours.
#======================================================================================
qiimeVSearchClusterFeaturesDeNovo () {
    qiime vsearch cluster-features-de-novo \
    --i-table $output_dada'Nat-Nep_p2_filtered-table.qza' \
    --i-sequences $output_dada'Nat-Nep_p2_filtered-seqs.qza' \
    --p-perc-identity .97 \
    --o-clustered-table $output_OTU'NN_p2_clustered_table' \
    --o-clustered-sequences $output_OTU'NN_p2_clustered_seqs' \
    --p-threads 10
}


#===  Function   ======================================================================
#        NAME: qiimeFeatureClassifierClassifySKLearn
# DESCRIPTION: Creates taxonomy table based on classifier
#      SBATCH: --cpus-per-task=5
#              --mem=12GB
#              --nodes=10
#              --ntasks-per-node=1
#         ETA: 3 hours.
#======================================================================================
qiimeFeatureClassifierClassifySKLearn () {
    qiime feature-classifier classify-sklearn \
    --i-reads $output_OTU'NN_p2_clustered_seqs.qza' \
    --i-classifier $classifier \
    --o-classification $output_taxonomy'NN_p2_taxonomy' \
    --p-n-jobs 10

    qiime tools export \
    --input-path $output_taxonomy'NN_p2_taxonomy.qza' \
    --output-path $output_taxonomy'NN_p2_taxonomy'
}

#===  Function   ======================================================================
#        NAME: qiimeTaxaCollapse
# DESCRIPTION: Collapse taxonomy table based on frequence table
#      SBATCH: --cpus-per-task=5
#              --mem=12GB
#              --nodes=10
#              --ntasks-per-node=1
#         ETA: 3 hours.
#======================================================================================
qiimeTaxaCollapse () {
    qiime taxa collapse \
    --i-table $output_OTU'Ank_p2_clustered_table.qza' \
    --i-taxonomy $output_taxonomy'Ank_p1-2_taxonomy.qza' \
    --p-level "Species" \
    --o-collapsed-table "taxonomic_annotated-table"
}


#===  Function   ======================================================================
#        NAME: qiimeAlignmentMafft
# DESCRIPTION: Aligns reads
#      SBATCH: --cpus-per-task=2
#              --mem=64GB
#              --nodes=20
#              --ntasks-per-node=1
#         ETA: 1 day.
#======================================================================================
qiimeAlignmentMafft () {
    qiime alignment mafft \
    --i-sequences $data_dir'merged-seqs.qza' \
    --o-alignment $output_alignment'aligned-seqs' \
    --p-parttree \
    --p-n-threads 2
}


#===  Function   ======================================================================
#        NAME: qiimePhylogenyFasttree
# DESCRIPTION: Creates the tree accourding to the Fasttree method based on aligned
#              reads.
#      SBATCH: --cpus-per-task=2
#              --mem=64GB
#              --nodes=20
#              --ntasks-per-node=1
#         ETA: 1-2 days.
#======================================================================================
qiimePhylogenyFasttree () {
    qiime phylogeny fasttree \
    --i-alignment $data_dir'aligned-seqs.qza' \
    --o-tree $data_dir'tree' \
    --p-n-threads 2
}


#===  Function   ======================================================================
#        NAME: qiimePhylogenyAlignToTreeMafftFasttree
# DESCRIPTION: Aligns reads with Mafft then creates the tree accourding to the Fasttree
#              method based on aligned reads.
#      SBATCH: --cpus-per-task=10
#              --mem=256GB
#	       --partition=himem
#              --nodes=1
#              --ntasks-per-node=1
#         ETA: < 1 hour.
#        NOTE: - phylogeny align-to-tree-mafft-fasttree can't handle >1 000 000
#                sequences, therefore mafft and fasttree can be ran seperately with
#                --o-parttree paremeter for Mafft
#	       - High memory usage
#======================================================================================
qiimePhylogenyAlignToTreeMafftFasttree () {
    qiime phylogeny align-to-tree-mafft-fasttree \
    --i-sequences $output_OTU'NN_p2_clustered_seqs.qza' \
    --o-alignment $output_alignment'NN_p2_aligned-seqs' \
    --o-masked-alignment $output_alignment'NN_p2_aligned-masked-seqs' \
    --o-tree $output_tree'NN_p2_tree' \
    --o-rooted-tree $output_tree'NN_p2_algined-tree' \
    --p-n-threads 10
}

#--------------------------------------------------------------------------------------
# Comment out any of the below function calls to run part of the script.
#--------------------------------------------------------------------------------------
# qiimeFeatureTableMergeTable
# qiimeFeatureTableMergeSeqs
# qiimeVSearchClusterFeaturesDeNovo
qiimeFeatureClassifierClassifySKLearn
# qiimeTaxaCollapse

# If sequences < 1 000 000 qiimePhylogenyAlignToTreeMafftFasttree can be used for ease.
qiimePhylogenyAlignToTreeMafftFasttree

# If sequences > 1 000 000 run qiimeAlignmentMafft and qiimePhylogenyFasttree.
# Uncomment the following two lines:
# qiimeAlignmentMafft
# qiimePhylogenyFasttree
